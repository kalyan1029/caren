# Caren

Caren RPM helps providers identify potential problems by remotely monitoring patients at home after hospital discharge and between clinic visits.

# Technologies used
- Java 11 Spring Boot Microservices
- Speech Recognition
- IOT sensors
- Cloud monitoring
- AWS
- Apache Kafka
- Angular 9
- Android

# Description
Caren RPM is an old age healthcare application used to
Record, Track and Share the health statistics of old age person

Caren RPM is a voice-interactive platform allows you to record, track and share the following:
- Blood pressure & Pulse
- %SpO2
- Respiration
- Weight
- Glucose
- Pain
- Temperature

Observations such as symptoms, photos, side effects, falls, ER visits

# Disclaimer
The code base is a private repo and client doesn't want the codebase to be disclosed,
I can explain the code base and flow of the app by screenshare or have a code review session
for some portions of the code

# Images

![](https://rodionsolutions.com/assets/images/caren/caren1.jpg)
![](https://rodionsolutions.com/assets/images/caren/caren2.jpg)
![](https://rodionsolutions.com/assets/images/caren/caren3.jpg)
![](https://rodionsolutions.com/assets/images/caren/caren4.jpg)
![](https://rodionsolutions.com/assets/images/caren/caren5.jpg)
![](https://rodionsolutions.com/assets/images/caren/caren6.jpg)
![](https://rodionsolutions.com/assets/images/caren/caren7.jpg)
![](https://rodionsolutions.com/assets/images/caren/caren8.jpg)

